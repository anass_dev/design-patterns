<?php 

namespace DP\Creational\Builder;

use DP\Creational\Builder\ClientBuilderInterface;
use DP\Creational\Builder\Models\InternationalClient;

Class InternationalClientBuilder implements ClientBuilderInterface
{

    private $Client;

    public function __construct()
    {
       
    }

    public function addReferenceNumber()
    {
        
    }

    public function addName()
    {
        
    }

    public function addAddress()
    {
        
    }

    public function addPhoneNumber()
    {

    }

    public function getClient()
    {

    }


   

}