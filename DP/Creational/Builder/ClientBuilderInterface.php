<?php 

namespace DP\Creational\Builder;

interface ClientBuilderInterface 
{

    public function addName();
    public function addReferenceNumber();
    public function addAddress();
    public function addPhoneNumber();

    public function getClient();
    
}