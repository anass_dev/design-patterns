<?php 

namespace Tests;

use DP\Creational\AbstractFactory\PhoneAbstractFactory;

use PHPUnit\Framework\TestCase;


class AnstractFactoryTest extends TestCase
{

    private $PhoneFactory;

    protected function setUp() : void
    {
        parent::setUp();
        $this->PhoneFactory = new PhoneAbstractFactory(); 
    }

    /*test */
    public function test_can_create_samsung_phone()
    {
        $samsung_phone = $this->PhoneFactory->createSamsung('galaxy',2000,'S5');

        $this->assertEquals('galaxy android is connected successfully',$samsung_phone->connect());
    }

}
