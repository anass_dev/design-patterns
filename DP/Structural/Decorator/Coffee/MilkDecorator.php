<?php 

namespace DP\Structural\Decorator\Coffee;

use DP\Structural\Decorator\Coffee\CoffeeDecorator;

class MilkDecorator implements CoffeeDecorator
{

    private $coffee;
    private $cost;
    private $description;

    public function __construct($coffee)
    {
        $this->coffee = $coffee;
    }

    public function getCost() : int
    {
        return $this->coffee->getCost() + 5;
    }

    public function getDescription()
    {
        return $this->coffee->getDescription() . ' with milk';
    }


}