<?php 

namespace Tests;

use DP\Structural\Decorator\Book\PrintBook;

use PHPUnit\Framework\TestCase;

class DecoratorBookTest extends TestCase 
{


    public function test_decorator_design_pattern_book_example()
    {
        $print_book = new PrintBook(trim('Learn Decorator'), trim('Junade martin'),trim('Develop robust and reusable code using a multitude of design patterns for PHP 7'));

        return $this->assertEquals( "Learn Decorator by Junade martin - Develop robust and reusable code using a multitude of design patterns for PHP 7", $print_book->getText() );

    }

}