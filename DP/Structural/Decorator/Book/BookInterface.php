<?php 

namespace DP\Structural\Decorator\Book;

interface BookInterface 
{

    public function getTitle  () : string ;
    public function getAuthor () : string ;
    public function getContent() : string ;

}