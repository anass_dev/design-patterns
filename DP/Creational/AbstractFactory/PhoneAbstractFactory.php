<?php 

namespace DP\Creational\AbstractFactory;

use DP\Creational\AbstractFactory\Samsung;
use DP\Creational\AbstractFactory\Iphone;

class PhoneAbstractFactory
{
    
    public function createSamsung($name,$price,$version) : Samsung
    {
        return new Samsung($name,$price,$version);
    }


    public function createIphone($name,$price,$version) : Iphone
    {
        return new Iphone($name,$price,$version);
    }


}