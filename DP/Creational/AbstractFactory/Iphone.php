<?php 

namespace DP\Creational\AbstractFactory;

use DP\Creational\AbstractFactory\Phone;

class Iphone implements PhoneInterface
{

    private $name;
    private $price;
    private $version;

    public function __construct($name,$price,$version)
    {

        $this->name     = $name;
        $this->price    = $price;
        $this->version  = $version;

    }

    public function connect() : string
    {
        return $this->name . ' ios is connected successfully';
    }

    public function shutdown() : string
    {
        return $this->name . ' ios is shutdown successfully';
    }

}