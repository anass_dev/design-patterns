<?php

namespace DP\Structural\Decorator\Book;

use DP\Structural\Decorator\Book\BookInterface;

use DP\Structural\Decorator\Book\EBook as EBook;


class PrintBook implements BookInterface
{
    
    private $ebook;

    
    public function __construct($title,$author,$content)
    {
 
        $this->ebook = new EBook($title,$author,$content);
    }


    public function getTitle() : string
    {
        return $this->ebook->getTitle();
    }


    public function getAuthor(): string
    {
        return $this->ebook->getAuthor();
    }


    public function getContent(): string
    {
        return $this->ebook->getContent();
    }


    public function getText() : string 
    {
        return $this->ebook->getTitle() . " by " . $this->ebook->getAuthor() 
                    . ' - ' 
                    . $this->getContent();
    }



}