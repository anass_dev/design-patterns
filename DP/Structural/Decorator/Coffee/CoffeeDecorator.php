<?php 

namespace DP\Structural\Decorator\Coffee;

use DP\Structural\Decorator\Coffee\Coffee as Coffee;

interface CoffeeDecorator 
{

    function __construct(Coffee $coffee);

    function getDescription();
    function getCost();

}