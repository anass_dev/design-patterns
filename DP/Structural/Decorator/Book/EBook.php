<?php 

namespace DP\Structural\Decorator\Book;

use DP\Structural\Decorator\Book\BookInterface;

class EBook implements BookInterface 
{

    private $title;
    private $content;
    private $author;

    
    public function __construct($title,$author,$content)
    {
        $this->title    = $title;
        $this->content  = $content;
        $this->author   = $author; 
    }


    public function getTitle() : string
    {
        return $this->title;
    }


    public function getAuthor(): string
    {
        return $this->author;
    }


    public function getContent(): string
    {
        return $this->content;
    }


}