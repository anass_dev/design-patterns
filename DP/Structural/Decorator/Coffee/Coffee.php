<?php 

namespace DP\Structural\Decorator\Coffee;

abstract class Coffee
{

    protected $cost = 5;
    protected $description;

    public function __construct($cost,$description)
    {
        
        $this->cost = $cost;
        $this->description = $description;

    }

    public function getCost()
    {
        return $this->cost;
    }
    
    public function getDescription()
    {
        return $this->description;
    }

}