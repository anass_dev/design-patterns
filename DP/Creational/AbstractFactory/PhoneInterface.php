<?php

namespace DP\Creational\AbstractFactory;


interface PhoneInterface
{
    public function connect();
    public function shutdown();

}