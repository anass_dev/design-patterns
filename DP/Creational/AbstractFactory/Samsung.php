<?php 

namespace DP\Creational\AbstractFactory;

class Samsung implements PhoneInterface
{

    private $name;
    private $price;
    private $version;

    public function __construct($name,$price,$version)
    {

        $this->name     = $name;
        $this->price    = $price;
        $this->version  = $version;

    }

    public function connect() : string
    {
        return $this->name . ' android is connected successfully';
    }

    public function shutdown() : string
    {
        return $this->name . ' android is shutdown successfully';
    }

}