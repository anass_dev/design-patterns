<?php 

namespace DP\Creational\Builder\Models;

abstract class Client
{

    private $reference_number;

    private $name;

    private $address;

    private $phone_number;


    public function __construct(int $reference_number, string $name, string $address,string $phone_number)
    {
        
        $this->reference_number = $reference_number;

        $this->name             = $name;

        $this->address          = $address;

        $this->phone_number     = $phone_number;

        
    }

}