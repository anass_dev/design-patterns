<?php 

namespace Tests;

use DP\Structural\Decorator\Coffee\MilkDecorator;
use DP\Structural\Decorator\Coffee\Espresso;
use PHPUnit\Framework\TestCase;

class DecoratorCoffeeTest extends TestCase 
{

    public function test_decorator_design_pattern_coffee_example()
    {
        $espresso_coffee = new Espresso(5,'espresso');

        $Milk_espresso_decorator = new MilkDecorator($espresso_coffee);

        $this->assertEquals(10,$Milk_espresso_decorator->getCost());
        $this->assertEquals('espresso with milk',$Milk_espresso_decorator->getDescription());

    }


}